# Monotiles

This project implements patterns for monotiles as published by
David Smith, Joseph Samuel Myers, Craig S. Kaplan, and Chaim
Goodman-Strauss.

* The original [An aperiodic monotile](https://arxiv.org/abs/2303.10798)
  that describes a tile shape, called a "Hat," that tiles the plane
  aperiodically but requires using some reflected (upside-down) tiles.
* The follow-up [A chiral aperiodic monotile](https://arxiv.org/abs/2305.17743)
  that describes a tile shape that *can* tile the plane aperiodically,
  if reflections are disallowed, and by modifying the edges into any of
  a class of shape they call a "Spectre," can tile the plane *only*
  aperiodically.

In practice, we found that printing the exact models of the tiles
failed to assemble perfectly because of printing artifacts. The
pattern would spread apart due to imprecision and artifacts, and
ultimately fail to tile.

To address this fault, each of these models is inset by 0.25mm
around the perimeter, and has all convex vertices filleted with a
0.5mm radius. This has, in practice, enabled a good fit enabling
substantial tiling. If, as printed on your printer, your tiles do
not pack well, you can modify the inset in the model to accommodate
your setup.

Each of these tiles is modeled in [FreeCAD](https://www.freecad.org/),
an Open Source CAD package, using essentially the same technique:
* Draw a sketch on the XY plane that expresses the nominal shape,
  fully constrained, with a single driving dimension (e.g. based on the
  length of a side).
* Use the Draft workbench 2D offset tool to create a -0.25mm offset
  (a 0.25mm inset) from that base sketch.
* Hide the sketch, open the Part Design workbench, select the offset, 
  and create a new body.
* In the new body, select the BaseFeature and pad it 3mm
* Select one edge representing a convex vertex, fillet, set 0.5mm
  radius, then select all other convex vertices.

If you want to modify these tiles, find the sketch inside the 2D offset
at the top of the model, find its contraints, and change the 15mm
BaseLength and/or (on the chiral models) the equation for the radius
of the arc sections that force chirality. Similarly, if with your printer,
you need additional inset, you can modify the 2D Offset to have a
larger inset, changing -0.25 to something of slightly higher magnitude.
Conversely, if you are printing this on a very well-tuned printer, you
might reduce it slightly so the tiles fit snugly.

It is best to print at least 8 or so tiles to test their fitment before
committing to a large print run.

## Einstein

The "EinStein" files model the original "Hat" shape
from the first paper. If you want to print different
colors for upright and flipped tiles, the proportions of
the colors to print [depend on the number of tiles you are
printing](https://puzzling.stackexchange.com/a/120346); for example,
for 48 normal tiles, print 7 flipped tiles. Alternatively,
you could print the bottom in one color and switch colors
halfway through printing, to make all tiles two-colored.

## Chiral

The second paper proposes a weakly chiral tile (chiral under fiat
forbidding flipping) which it calls "Tile(1,1)" (here, files named
"WeakChiral"), and two examples for side modifications that force
chirality. One with dual-curved, S-shaped edges, and one with single
curved, C-shaped edges. These strongly chiral tiles the paper calls
"Spectres".  For the purpose of file naming, The S-shaped edges
are called "SSpectre" and the C-shaped edges are called "speCCtre"
(the letter is doubled so that the files do not conflict on Windows
systems that do not respect case in file name). These C-curved and
S-curved shapes will not mesh inverted, and so enforce the fiat not
to flip them over.

-----------------------

Don't want to print them? See this delightful laser-cut puzzle instead:
https://n-e-r-v-o-u-s.com/blog/?p=9333

-----------------------

Inasmuch as any copyright applies to these mathematically-defined
files, use them under CC0 terms:
https://creativecommons.org/publicdomain/zero/1.0/
